/////////////////////////////////////
// Building in Decentraland course
// Lesson 9 scene to fix
// 
// this scene started out as a DCL 2.2.6, SDK 5.0.0 "dcl init" scene, as follows:
/*
npm rm -g decentraland
npm i -g decentraland@2.2.5
md Lesson9a-SDK5-FixMe
cd Lesson9a-SDK5-FixMe
dcl init
npm rm decentraland-ecs
npm i decentraland-ecs@5.0.0
// imported an SDK5 version of spawnerFunctions.ts module
// got busy reworking the game.ts file to have the basis for the Lesson assignment
*/

import {spawnPlaneX, spawnBoxX, spawnGltfX} from './modules/spawnerFunctions'

const ground = new Entity()
ground.addComponent(new Transform({
    position: new Vector3(8,0,8),
    scale: new Vector3(16,16,.1),
    rotation: new Quaternion(1,0,0,1)
}))
ground.addComponent(new BoxShape())
const myTexture = new Texture('models/TX/paintwall_tx.png')
const groundMaterial = new Material()
groundMaterial.albedoTexture = myTexture
ground.addComponent(groundMaterial)
engine.addEntity(ground)



const box = new Entity()
box.addComponent(new Transform({
    position: new Vector3(4,1,7),
    scale: new Vector3(1,1,1),
    rotation: new Quaternion(0,180,0,1)
}))
box.addComponent(new BoxShape())
const myMaterial = new Material()
myMaterial.albedoColor = new Color3(0,1,0)
myMaterial.metallic = 0.95
myMaterial.roughness = 0.05
box.addComponent(myMaterial)
engine.addEntity(box)

const collider = new Entity()
collider.addComponent(new Transform({
    position: new Vector3(4,1,7),
    scale: new Vector3(.5,.5,.5),
    rotation: new Quaternion(0,0,0,1)

}))
collider.addComponent(new GLTFShape('models/Lesson9Cube/collider_fixed.glb'))
engine.addEntity(collider)


const cube = new Entity()
cube.addComponent(new GLTFShape('models/Lesson9Cube/cube_fixed.glb'))
cube.addComponent(new Transform({
    position: new Vector3 (4,1,8),
    scale: new Vector3 (.5,.5,.5),
    rotation: new Quaternion(0,180,0,1)
}))
engine.addEntity(cube)


const naturePack = new Entity()
naturePack.addComponent(new GLTFShape('models/NaturePack/tree_fixed.glb'))
naturePack.addComponent(new Transform({
    position: new Vector3(7,0,5),
    scale: new Vector3(.3,.3,.3),
    rotation: new Quaternion(0,180,0,1)
}))
engine.addEntity(naturePack)

const shrub = new Entity()
shrub.addComponent(new GLTFShape('models/NaturePack/shrub_fixed.glb'))
shrub.addComponent(new Transform({
    position: new Vector3(8,.1,5),
    scale: new Vector3(.2,.2,.2),
    rotation: new Quaternion(0,0,0,1)
})
engine.addEntity(shrub)

const shrub = new Entity()
shrub.addComponent(new GLTFShape('models/NaturePack/shrub_fixed.glb'))
shrub.addComponent(new Transform({
    position: new Vector3(6,0,4),
    scale: new Vector3(.2,.2,.2),
    rotation: new Quaternion(0,0,0,1)
})
engine.addEntity(shrub)


const firTree = new Entity()
firTree.addComponent(new GLTFShape('models/TreeA_Fir/treeA_fixed.glb'))
firTree.addComponent(new Transform({
    position: new Vector3(4,0,4),
    scale: new Vector3(.1,.1,.1),
    rotation: new Quaternion(0,180,0,1)
}))
engine.addEntity(firTree)


const bird = new Entity()
bird.addComponent(new GLTFShape('models/Sparrow/Sparrow-burung-pipit.gltf'))
bird.addComponent(new Transform({
    position: new Vector3(8,1,8),
    scale: new Vector3(.1,.1,.1),
    rotation: new Quaternion(0,180,0,1)
}))
const animator = new Animator()
bird.addComponent(animator)
let clipfly = new AnimationState("fly")
animator.addClip(clipfly)
clipfly.play()

engine.addEntity(bird)








